# BookSure - Hotel Reservation Cancellation Prediction 🔮
<figure style="text-align: center;">
    <img src="docs/application.png" alt="Description" width="400" height="400" />
</figure>

## Project Objective 💡
The purpose of BookSure is to deliver a cancellation prediction tool that allows hoteliers to :
- Identify reservations likely to be canceled.
- Better plan human and material resource management.
- Minimize financial losses caused by cancellations.
- Confidently apply overbooking strategies.

## Key Features ⚙️
- **Cancellation Prediction** : Analyze data to predict whether a reservation will be canceled or honored.
- **Data Input** :
  - Manual Input : A form to enter reservation details.
- **Results Visualization** : Clear display of predictions to support decision-making.

## Dataset 📚

### Data Source
The dataset used in this project is sourced from the article :
> Nuno Antonio et al., “Hotel booking demand datasets”, *Data in Brief*, Volume 22, 2019.  
You can access the database via the link : [Hotel booking demand](https://www.kaggle.com/datasets/jessemostipak/hotel-booking-demand)

### Description
- **Period** : Reservations between July 2015 and August 2017.
- **Location** : Data collected from hotels in Portugal.
- **Observations** : 119 390.
- **Variables** : 31 features detailing each reservation (date, duration, number of guests, etc.).
- **Target variable** : `is_canceled` (1: canceled, 0: confirmed).


## Setup and Usage 🧩
Before using the BookSure application, ensure you have the following:

### Requirements
- **Python 3.x** 🐍 : Download and install from [Python.org](https://www.python.org/).
- **Microsoft Excel** 📝 : Required for the macro-based Excel user interface.

### Installation
1. Clone the Git repository to your local machine :
   ```bash
   git clone https://gitlab-mi.univ-reims.fr/cola0023/projet_creation_application.git
      
2. Navigate to the cloned directory :
   ```bash
   cd projet_creation_application
3. Install or update pip : 
   ```bash
   python.exe -m pip install --upgrade pip
4. Install the required dependencies using pip :
   ```bash
   pip install -r requirements.txt 
## Repository Structure 📂
The repository is organized as follows :
1. **`data/`**  📂
   - Contains the datasets used for the project. 
     - `hotel_boking.csv` : Raw dataset before processing.  
     - `data_clean.csv` : Cleaned dataset ready for analysis 
     - `balanced_data.csv` : Rebalanced dataset used for the predictive models.

2. **`docs/`**  📂
   - Includes documentation and demonstration files related to the project.  
     - For example : Demo, reports, or illustrations.  

3. **`interface/`**  📂
   - Houses the Excel file used as the user interface for the application.  
     - `app.xlsm` : An interactive interface using Excel macros.  

4. **`models/`**  📂
   - Contains the tested prediction models, as well as modelling using R programming language.
 

5. **`script/`**  📂
   - Contains Python scripts for various project stages.  
     - The "run_prediction" folder that is triggered by VBA to perform the prediction, another folder with additional data preparation, etc.

6. **`tests/`**  📂
   - Hosts unit tests to ensure code reliability and robustness.
     - These tests validate the application's key functionalities.  
7. README.md 📄:A document describing the project's organization and usage.
8. requirements.txt 📄:Lists the Python packages needed to run the application.
## Project Usage 💻  
To use our hotel reservation cancellation prediction application, follow these steps:
1. **Launch the application in Excel**  
   - Navigate to the interface folder. Open the Booksure_app.xlsm file to start the application. Make sure to enable macros for proper functionality. If needed, right-click on the file, go to Properties, and check the Unblock box at the bottom of the General tab.  

2. **Fill out the form**
   - Click on the « commencer » button to open the form. The form is divided into two pages, and you will need to complete the required fields on both. The first page focuses on reservation details, while the second page asks for customer-related information. Make sure to fill out all the fields accurately.  

3. **Predict cancellations** 
   - After entering the necessary information, the data will first be synthesized and displayed for review to ensure its accuracy. Once you've confirmed the information, click on the « lancer la prédiction » button. The prediction model, will automatically run in the background, calculating the probability of cancellation based on the provided data. 

4. **View the results**   
   - You’ll be prompted to wait a few seconds as the cancellations are processed. Once the prediction is complete, the cancellation probability will appear, along with additional elements for a more attractive experience.
   
## Contributors 🤝
The application was developed by students of the Master's program in « Statistique pour l’Évaluation et la Prévision » at the University of Reims Champagne-Ardenne, Class of 2024–2025. Below are the contributors and their respective roles :
- **Baya BENOURDJA** : Product Owner
- **Lucile SAILLANT** : Front End/User Interface - Scrum Master
- **Lucas COLARD** : Data Engineer - Scrum Master
- **Rodolphe GABA** : Data Scientist
- **Clara RAKOTOARISOA** : Data Analyst




