#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Dec  9 11:37:18 2024

@author: LucasCld7
"""

import os
import sys

try:
    sys.path.append(os.path.join(os.getcwd(), ".."))
    from run_analyse_or_cleaning import run_analyse_before_cleaning
    sys.path.append(os.path.join(os.getcwd(), "..", ".."))
    from data_analysis_viz import load_data, path_where
    print("Modules importés avec succès.")
except ImportError as e:
    print(f"Erreur d'importation : {e}")

if __name__ == "__main__":
    
    # Get the current file path
    Path = path_where()
    
    # Path for loading and saving data
    hotel_bookings = os.path.join(Path, "..", "..", "..", "data", "hotel_bookings.csv")
    path_to_save = os.path.join(Path, "..", "..", "..", "data", "data_clean.csv")
    
    # Load dataset
    df = load_data(hotel_bookings)
    
    # Run one of the analyse
    run_analyse_before_cleaning(df)