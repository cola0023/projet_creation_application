import pandas as pd
import matplotlib.pyplot as plt

def describe_object_columns(df: pd.DataFrame) -> None:
    """
    Display summary statistics for object-type columns and list unique values for each.
    
    Parameters:
        df (pd.DataFrame): The DataFrame containing the data.
        
    Returns:
        None
    """
    print("Summary statistics for categorical columns:\n", df.describe(include='object'))
    for col in df.describe(include='object').columns:
        print(f"Column: {col}\nUnique values: {df[col].unique()}\n" + '-' * 50)

def percent_missing_values(df: pd.DataFrame) -> pd.Series:
    """
    Calculate and return the percentage of missing values in each column.
    
    Parameters:
        df (pd.DataFrame): The DataFrame containing the data.
        
    Returns:
        pd.Series: A Series containing the percentage of missing values for each column.
    """
    percent_na = df.isnull().sum() * 100 / len(df)
    percent_na_var = percent_na[percent_na > 0].sort_values(ascending=False)
    return round(percent_na_var, 3)

def handle_missing_values(df: pd.DataFrame) -> pd.DataFrame:
    """
    Handle missing values by removing rows and converting missing data.
    
    Parameters:
        df (pd.DataFrame): The DataFrame containing the data.
        
    Returns:
        pd.DataFrame: The DataFrame with missing values handled.
    """
    missing_percent = percent_missing_values(df)
    print("Missing values percentage:\n", missing_percent)
    i = 0
    generated_columns = []
    while (missing_percent.iloc[i] > 5) :
        df[f"used_{missing_percent.index[i]}"] = df[missing_percent.index[i]].apply(lambda x: 0 if pd.isna(x) else 1)
        generated_columns.append(missing_percent.index[i])
        i += 1
    # Check unique values for object-type columns
    # describe_object_columns(df)
    df = handle_manualy(df, 'meal', 'Undefined', 'SC')
    df = df.drop(columns = generated_columns)
    df = df.dropna()
    return df

def handle_manualy(df: pd.DataFrame, variable: str, modif_value: str, replace_value: str) -> pd.DataFrame:
    """
    Replace a specific value in a column with another value.
    
    Parameters:
        df (pd.DataFrame): The DataFrame containing the data.
        variable (str): The column to replace values in.
        modif_value (str): The value to replace.
        replace_value (str): The value to replace it with.
    
    Returns:
        pd.DataFrame: The DataFrame with the specified value replaced.
    """
    df[variable] = df[variable].replace(modif_value, replace_value)
    return df
    

def handle_duplicates(df: pd.DataFrame) -> pd.DataFrame:
    """
    Remove duplicate rows from the DataFrame.
    
    Parameters:
        df (pd.DataFrame): The DataFrame containing the data.
    
    Returns:
        pd.DataFrame: The DataFrame with duplicate rows removed.
    """
    before = df.shape[0]
    df = df.drop_duplicates()
    after = df.shape[0]
    print(f"Duplicates removed: {before - after}")
    return df

def handle_inconsistent_data(df: pd.DataFrame) -> pd.DataFrame:
    """
    Remove rows with inconsistent or logically incorrect data.
    
    Parameters:
        df (pd.DataFrame): The DataFrame containing the data.
    
    Returns:
        pd.DataFrame: The DataFrame with inconsistent data removed.
    """
    no_guest = (df['babies'] == 0) & (df['children'] == 0) & (df['adults'] == 0)
    df = df[~no_guest]
    return df

def handle_outliers(df: pd.DataFrame) -> pd.DataFrame:
    """
    Cap outliers for specified numerical columns.
    
    Parameters:
        df (pd.DataFrame): The DataFrame containing the data.
        
    Returns:
        pd.DataFrame: The DataFrame with outliers capped.
    """
    df.loc[df['required_car_parking_spaces'] > 5, 'required_car_parking_spaces'] = 5
    df = df.drop(df['adr'].idxmax())
    return df


def convert_data_types(df: pd.DataFrame, variable1: str, variable2: str) -> pd.DataFrame:
    """
    Converts data types for specified columns in a DataFrame.

    Parameters:
        df (DataFrame): The DataFrame whose columns need type conversion.
        variable1 (str): The first column to convert to integer.
        variable2 (str): The second column to convert to datetime.

    Returns:
        DataFrame: The DataFrame with updated data types for specified columns.
    """
    df.loc[:, variable1] = df[variable1].astype(int)
    df.loc[:, variable2] = pd.to_datetime(df[variable2])
    return df

def plot_boxplots(df: pd.DataFrame, num_features: list, title: str) -> None:
    """
    Generate boxplots for numerical features.
    
    Parameters:
        df (pd.DataFrame): The DataFrame containing the data.
        num_features (list): The numerical features to plot.
        title (str): The title of the plot.
    
    Returns:
        None
    """
    plt.figure(figsize=(20, 10))
    plt.suptitle(title)
    for i, feature in enumerate(num_features, 1):
        plt.subplot(4, 4, i)
        df.boxplot(column=feature, vert=True)
    plt.tight_layout()

