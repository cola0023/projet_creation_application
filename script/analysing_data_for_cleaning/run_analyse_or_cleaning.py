#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Dec  9 11:37:38 2024

@author: LucasCld7
"""

import matplotlib.pyplot as plt
import pandas as pd

try:
    import data_cleaning as dc
    print("Modules importés avec succès.")
except ImportError as e:
    print(f"Erreur d'importation : {e}")

def run_analyse_before_cleaning(df: pd.DataFrame) -> None:
    """
    Run the data analysis workflow before cleaning the data.
    
    Parameters:
        df (DataFrame): The DataFrame containing reservation data.
        
    Returns:
        None
    """
    print("Original dimensions:", df.shape)
    
    # Display initial information
    print("Dataset info:\n", df.info())
    print("First 5 rows of the dataset:\n", df.head())
    
    # Handle missing values
    df1 = dc.handle_missing_values(df)
        
    # Remove rows with inconsistent data
    df2 = dc.handle_inconsistent_data(df1)
    
    # Convert data types as required
    df3 = dc.convert_data_types(df2, 'children', 'reservation_status_date')
    
    # Identify and cap outliers
    num_features = [
        'lead_time', 'stays_in_weekend_nights', 'stays_in_week_nights', 
        'adults', 'children', 'babies', 'previous_cancellations', 
        'previous_bookings_not_canceled', 'booking_changes', 
        'days_in_waiting_list', 'adr', 'required_car_parking_spaces', 
        'total_of_special_requests'
    ]
    
    # Ploting boxplots for analysing
    dc.plot_boxplots(df3, num_features, "Boxplots of numerical features (before outlier handling)")
    df4 = dc.handle_outliers(df3)
    dc.plot_boxplots(df4, num_features, "Boxplots of numerical features (after outlier handling)")
    
    plt.show()
    
def run_saving_cleaning_data(df: pd.DataFrame, path_to_save: str) -> None:
    """
    Run the data cleaning workflow and save the cleaned data.
    
    Parameters:
        df (DataFrame): The DataFrame containing reservation data.
        path_to_save (str): The path to save the cleaned data.
    
    Returns:
        None
    """ 
    # Handle missing values
    df1 = dc.handle_missing_values(df)
        
    # Remove rows with inconsistent data
    df2 = dc.handle_inconsistent_data(df1)
    
    # Convert data types as required
    df3 = dc.convert_data_types(df2, 'children', 'reservation_status_date')
    
    # Cap outliers
    df4 = dc.handle_outliers(df3)
    
    # Remove duplicate entries
    data = dc.handle_duplicates(df4)
    
    # Save the cleaned data
    data.to_csv(path_to_save, index=False)