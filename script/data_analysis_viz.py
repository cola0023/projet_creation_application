import pandas as pd
import matplotlib.pyplot as plt
import os

def path_where() -> str:
    """
    Determines the current working directory of the file.

    Parameters:
        None

    Returns:
        str: The absolute path of the current working directory.
    """
    Path = os.getcwd()
    return Path

def load_data(file_path: str) -> pd.DataFrame:
    """ 
    Loads a dataset from a CSV file and returns it as a pandas DataFrame.

    Parameters:
        file_path (str): Path to the CSV file.

    Returns:
        DataFrame: A pandas DataFrame containing the dataset.
    """
    df = pd.read_csv(file_path, na_values='NULL')
    return df

def convert_data_types(df: pd.DataFrame, variable1: str, variable2: str) -> pd.DataFrame:
    """
    Converts data types for specified columns in a DataFrame.

    Parameters:
        df (DataFrame): The DataFrame whose columns need type conversion.
        variable1 (str): The first column to convert to integer.
        variable2 (str): The second column to convert to datetime.

    Returns:
        DataFrame: The DataFrame with updated data types for specified columns.
    """
    df[variable1] = df[variable1].astype(int)
    df[variable2] = pd.to_datetime(df[variable2])
    return df

def calculate_stats(df: pd.DataFrame, variable: str) -> pd.DataFrame:
    """
    Calculates counts and percentages for a given variable.

    Parameters:
        df (DataFrame): The DataFrame containing the data.
        variable (str): The column for which to calculate statistics.

    Returns:
        DataFrame: A DataFrame with counts and percentages for the specified column.
    """
    stats_counts = df[variable].value_counts()
    stats_perc = stats_counts * 100 / len(df)
    return pd.DataFrame({'count': stats_counts, 'percentage': round(stats_perc, 2)})

def plot_pie_chart(df: pd.DataFrame, colname: str, labels: list, title: str, colors: list) -> None:
    """ 
    Plots a pie chart based on a specified column in the dataframe. 
    
    Parameters: 
        df (DataFrame): The data to plot.
        colname (str): The column name to use for the pie chart.
        labels (list): The labels for the pie chart.
        title (str): The title of the pie chart.
        colors (list): The colors for the pie chart.
    
    Returns:
        None
    """
    if colname not in df.columns:
        raise ValueError(f"Column '{colname}' not found in the DataFrame.")
    
    # Display canceled counts and percentages
    df_stats = calculate_stats(df, 'is_canceled')
    print("\n***", title, "***\n",  df_stats)
    
    data_count = df[colname].value_counts()  # Count occurrences of each unique value
    plt.figure(figsize=(7, 7))
    patches, texts, pcts = plt.pie(
        data_count, labels = labels, autopct = '%1.1f%%',
        colors = colors, startangle = 90,
        wedgeprops = {'linewidth': 2.0, 'edgecolor': 'white'}, 
        textprops = {'size': 'x-large'}
    )                
    plt.title(title, fontsize=14, fontweight = 'bold')
    plt.setp(pcts, color='white', fontweight = 'bold')  # Set the color of the labels to white
    plt.tight_layout()

def plot_hotel_cancellation(df: pd.DataFrame, title: str) -> None:
    """
    Plots hotel cancellation status percentages.

    Parameters:
        df (DataFrame): The DataFrame containing reservation data.
        title (str): The title of the plot.

    Returns:
        None
    """
    # Create a pivot table for hotel cancellation counts
    hotel_cancel_counts = df.pivot_table(index='hotel', columns='is_canceled', aggfunc='size', fill_value=0)
    hotel_cancel_counts.columns = ['not_cancelled', 'cancelled']

    # Calculate percentages
    hotel_cancel_perc = hotel_cancel_counts.div(hotel_cancel_counts.sum(axis=1), axis=0) * 100

    print("\n*** Count of Hotel Cancellation Status ***\n", hotel_cancel_counts)
    print("\n*** Percentage of Cancellation Status ***\n", round(hotel_cancel_perc, 2))

    # Define colors for the bars
    colors = ['#ff27c3', '#2B4563']

    # Create the plot
    ax = hotel_cancel_counts.plot(kind='bar', color=colors, figsize=(10, 6))
    plt.title(title, fontsize=14, fontweight='bold')
    plt.xlabel('Hotel Type', fontsize=12)
    plt.ylabel('Reservation Count', fontsize=12)
    plt.legend(title="Cancellation Status")

    # Add percentage annotations on each bar
    for i, bars in enumerate(ax.containers):
        ax.bar_label(
            bars,
            labels=[f'{p:.2f}%' for p in hotel_cancel_perc.iloc[:, i]],
            label_type='edge',
            fontsize=10,
            fontweight='bold',
            color='black'
        )

    # Customize ticks and grid
    plt.xticks(rotation=0, fontsize=12, color='black')
    plt.yticks(fontsize=12, color='black')
    plt.grid(axis='y', linestyle='--', alpha=0.7)
    plt.tight_layout()

def plot_all(df: pd.DataFrame, title_suffix: str, type_cleaning: int) -> None:
    """
    Plots all visualizations for a given dataset.

    Parameters:
        df (DataFrame): The DataFrame containing reservation data.
        title_suffix (str): The title suffix for each plot.
        type_cleaning (int): 0 for "Before Cleaning" and 1 for "After Cleaning".

    Returns:
        None
    """
    if type_cleaning == 0:
        print("\n======= Before Cleaning =======\n")
    else:
        print("\n======= After Cleaning =======\n")
    
    plot_pie_chart(
        df = df,
        colname = 'is_canceled',
        labels=['Not Cancelled', 'Cancelled'],
        title=f'Reservation Status {title_suffix}',
        colors = ['#ff27c3', '#660f4e']
    )
    
    plot_pie_chart(
        df = df,
        colname = 'hotel',
        labels=df['hotel'].unique(),
        title=f'Hotel Type {title_suffix}',
        colors = ['#f76c5e', '#9b2d20']
    )
    
    plot_hotel_cancellation(df, f'Hotel Cancellation Status {title_suffix}')
    plt.show()

def run_visualisation() -> None:
    """
    Runs the visualization workflow by loading datasets, processing them, and plotting visualizations.
    """
    # Get the current file path
    Path = path_where()
    
    # Define file paths for raw and cleaned data
    hotel_bookings = os.path.join(Path, "..", "data", "hotel_bookings.csv")
    data_clean = os.path.join(Path, "..", "data", "data_clean.csv")
    
    # Load datasets
    raw_data = load_data(hotel_bookings)
    cleaned_data = load_data(data_clean)

    # Convert data types for cleaned data
    cleaned_data = convert_data_types(cleaned_data, 'children', 'reservation_status_date')

    # Plot graphs for raw and cleaned data
    plot_all(raw_data, 'Before Cleaning', 0)   
    plot_all(cleaned_data, 'After Cleaning', 1)

if __name__ == "__main__":
    """
    Main function to execute the visualization workflow.
    """
    run_visualisation()
