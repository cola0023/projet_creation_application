#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Dec  9 15:02:07 2024

@author: LucasCld7
"""

import os
import sys

# The next line is not strictly necessary, but it ensures the system can locate the file more efficiently
sys.path.append(os.path.join(os.getcwd(), "..", "script", "prediction_file")) 
from run import run_predict

if __name__ == "__main__":
    """
    Entry point of the script.

    Description:
        - When the script is executed directly, this block is triggered.
        - It calls the `run_predict` function to perform the prediction workflow.

    """

    run_predict()  # Execute the prediction workflow
