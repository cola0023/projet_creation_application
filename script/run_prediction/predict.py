import pandas as pd
import pickle
import os

def make_prediction(entry: pd.DataFrame) -> int:
    """
    Predicts the outcome using a pre-trained Random Forest model.

    Parameters:
        entry (pd.DataFrame): A one-row DataFrame containing the columns required by the model.

    Returns:
        int: The prediction result (e.g., 0 or 1) obtained from the Random Forest model.

    Details:
        - The function loads a pre-trained Random Forest model stored in a pickle file.
        - The model file is located in the `models` directory, relative to the current working directory.
        - The model's `predict` method is used to make a prediction based on the input data.
    """ 
    with open(os.path.join(os.getcwd(), "..", "models", "randomForest_v4.pkl"), "rb") as file:
        model = pickle.load(file)  # Load the pre-trained Random Forest model
        return model.predict_proba(entry)[0][1] # Predict the outcome for the input entry

def import_data() -> tuple:
    """
    Imports the input data for prediction.

    Parameters:
        None

    Returns:
        tuple:
            - str: The absolute path of the current working directory.
            - pd.DataFrame: A DataFrame containing the input data loaded from a CSV file.

    Details:
        - The input CSV file is expected to be located in the `interface` directory.
        - The function constructs the file path dynamically relative to the current working directory.
    """
    # Get the current working directory
    path = os.getcwd()
    
    # Construct the file path for the input CSV file
    input_file = os.path.join(path, "input.csv")
    
    # Read the CSV file into a DataFrame
    df = pd.read_csv(input_file)
    
    return path, df

def arrange_df(df: pd.DataFrame) -> pd.DataFrame:
    """
    Arranges the DataFrame columns in a specific order required for prediction.

    Parameters:
        df (pd.DataFrame): The DataFrame to be rearranged.

    Returns:
        pd.DataFrame: The rearranged DataFrame with columns in the required order.

    Details:
        - The function ensures that the DataFrame has the necessary columns in the correct order
          for the Random Forest model.
        - Columns not in the `required_columns` list are removed from the DataFrame.
    """
    # Define the required column order for prediction
    required_columns = [
        'lead_time', 'stays_in_week_nights', 'adults', 'children',
        'market_segment', 'previous_cancellations', 'previous_bookings_not_canceled', 
        'booking_changes', 'deposit_type', 'customer_type', 'adr', 'total_of_special_requests',
        'used_company', 'reserved_is_assigned'
    ]

    # Rearrange the DataFrame to match the required column order
    df = df[required_columns]

    return df

def predict_and_save(df: pd.DataFrame, path: str) -> None:
    """
    Predicts the outcome for the input data and saves the result to a file.

    Parameters:
        df (pd.DataFrame): The input DataFrame containing the data for prediction.
        path (str): The current working directory path.

    Returns:
        None

    Details:
        - The function makes a prediction using the `make_prediction` function.
        - The prediction result is written to an output text file located in the `interface` directory.
    """
    # Make the prediction for the input data
    prediction = make_prediction(df)
    
    # Construct the normalize file path for the output file
    output_file = os.path.normpath(os.path.join(path, "output.txt"))
    
    # Write the prediction result to the output file
    with open(output_file, "w") as file:
        file.write(str(prediction))
