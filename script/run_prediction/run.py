
import os
import sys

# The next line is not strictly necessary, but it ensures the system can locate the file more efficiently
sys.path.append(os.path.join(os.getcwd(), "..", "script", "prediction_file"))
import predict as pr  # Import the predict module containing prediction-related functions

def run_predict() -> None:
    """
    Executes the full prediction workflow.

    Workflow steps:
        1. Import the input data and retrieve the current working directory.
        2. Arrange the DataFrame columns to match the required format for prediction.
        3. Perform the prediction using the logistic regression model and save the result to a file.

    Parameters:
        None

    Returns:
        None
    """
    # Import the input data and get the current working directory path
    path, reservation = pr.import_data()
    
    # Arrange the DataFrame columns for the prediction
    reservation = pr.arrange_df(reservation)
    
    # Make the prediction and save the result
    pr.predict_and_save(reservation, path)