import pytest
import pandas as pd
import os
import matplotlib.pyplot as plt
from script.data_analysis_viz import *  # Import all functions


def test_path_where() -> None:
    """
    Tests if the `path_where` function returns the correct current working directory.

    Returns:
        None
    """
    expected_path: str = os.getcwd()
    result: str = path_where()
    assert result == expected_path, f"The returned path ({result}) does not match the expected path ({expected_path})."


def test_load_data() -> None:
    """
    Tests if the `load_data` function correctly loads a CSV file into a DataFrame.

    Returns:
        None
    """
    test_csv_content: str = "col1,col2,col3\n1,2,3\n4,5,6\n7,8,NULL"
    test_file_path: str = "test_data.csv"
    with open(test_file_path, "w") as f:
        f.write(test_csv_content)

    df: pd.DataFrame = load_data(test_file_path)

    expected_df: pd.DataFrame = pd.DataFrame({
        "col1": [1, 4, 7],
        "col2": [2, 5, 8],
        "col3": [3, 6, None]
    })

    pd.testing.assert_frame_equal(df, expected_df)
    os.remove(test_file_path)


def test_convert_data_types() -> None:
    """
    Tests if the `convert_data_types` function correctly converts the data types
    of the specified columns.

    Returns:
        None
    """
    data: dict = {
        "var1": ["1", "2", "3"],
        "var2": ["2023-01-01", "2023-01-02", "2023-01-03"]
    }
    df: pd.DataFrame = pd.DataFrame(data)

    # Apply type conversion
    result_df: pd.DataFrame = convert_data_types(df, "var1", "var2")

    # Expected DataFrame after conversion
    expected_df: pd.DataFrame = pd.DataFrame({
        "var1": [1, 2, 3],  # Should be integers
        "var2": pd.to_datetime(["2023-01-01", "2023-01-02", "2023-01-03"])  # Should be datetime
    })

    pd.testing.assert_frame_equal(result_df, expected_df)


def test_calculate_stats() -> None:
    """
    Tests if the `calculate_stats` function computes correct statistics for a given column.

    Returns:
        None
    """
    data: dict = {"category": ["A", "B", "A", "A", "B", "C"]}
    df: pd.DataFrame = pd.DataFrame(data)

    # Apply the calculate_stats function
    stats_df: pd.DataFrame = calculate_stats(df, "category")

    # Expected DataFrame
    expected_stats_df: pd.DataFrame = pd.DataFrame({
        "count": [3, 2, 1],
        "percentage": [50.0, 33.33, 16.67]
    }, index=["A", "B", "C"])
    expected_stats_df.index.name = "category"

    pd.testing.assert_frame_equal(stats_df, expected_stats_df)


def test_plot_pie_chart_column_not_found() -> None:
    """
    Tests if the `plot_pie_chart` function raises a `ValueError` when the specified column is not found.

    Returns:
        None
    """
    # Create a test DataFrame
    df: pd.DataFrame = pd.DataFrame({"col1": [1, 2, 3], "col2": ["A", "B", "A"]})

    # Test if the specified column does not exist
    with pytest.raises(ValueError, match="Column 'col_missing' not found in the DataFrame."):
        plot_pie_chart(df, "col_missing", ["A", "B"], "Test Title", ["red", "blue"])


def test_plot_pie_chart_value_counts() -> None:
    """
    Tests if the `plot_pie_chart` function correctly computes occurrences for the specified column.

    Returns:
        None
    """
    # Create a test DataFrame
    df: pd.DataFrame = pd.DataFrame({"category": ["A", "B", "A", "A", "B", "C"]})

    # Extract data for the chart
    colname: str = "category"
    data_count: pd.Series = df[colname].value_counts()
    data_count.index.name = "category"  # Explicitly name the index
    data_count.name = "count"  # Explicitly name the series

    # Expected data
    expected_counts: pd.Series = pd.Series(
        [3, 2, 1],
        index=["A", "B", "C"],
        name="count"  # Ensure the name matches
    )
    expected_counts.index.name = "category"

    # Validate results
    pd.testing.assert_series_equal(data_count, expected_counts)


def test_plot_hotel_cancellation_pivot_table() -> None:
    """
    Tests if the pivot table for hotel cancellations is correctly generated.

    Returns:
        None
    """
    # Input data
    data: dict = {
        "hotel": ["Resort", "Resort", "City", "City", "Resort"],
        "is_canceled": [1, 0, 1, 0, 1]
    }
    df: pd.DataFrame = pd.DataFrame(data)

    # Expected pivot table
    expected_pivot: pd.DataFrame = pd.DataFrame(
        {
            "not_cancelled": [1, 1],
            "cancelled": [2, 1]
        },
        index=["Resort", "City"]
    )
    expected_pivot.index.name = "hotel"  # Add the expected index name

    # Generate pivot table in the function
    hotel_cancel_counts: pd.DataFrame = df.pivot_table(index='hotel', columns='is_canceled', aggfunc='size', fill_value=0)
    hotel_cancel_counts.columns = ['not_cancelled', 'cancelled']

    # Sort DataFrames by index before comparison
    hotel_cancel_counts = hotel_cancel_counts.sort_index()
    expected_pivot = expected_pivot.sort_index()

    # Validate results
    pd.testing.assert_frame_equal(hotel_cancel_counts, expected_pivot)


def test_plot_hotel_cancellation_percentage() -> None:
    """
    Tests if the percentages of hotel cancellations and non-cancellations are correctly computed.

    Returns:
        None
    """
    # Input data
    data: dict = {
        "hotel": ["Resort", "Resort", "City", "City", "Resort"],
        "is_canceled": [1, 0, 1, 0, 1]
    }
    df: pd.DataFrame = pd.DataFrame(data)

    # Expected pivot table
    hotel_cancel_counts: pd.DataFrame = pd.DataFrame(
        {
            "not_cancelled": [1, 1],
            "cancelled": [2, 1]
        },
        index=["Resort", "City"]
    )

    # Expected percentages
    expected_perc: pd.DataFrame = pd.DataFrame(
        {
            "not_cancelled": [33.33, 50.0],
            "cancelled": [66.67, 50.0]
        },
        index=["Resort", "City"]
    )

    # Compute percentages in the function
    hotel_cancel_perc: pd.DataFrame = hotel_cancel_counts.div(hotel_cancel_counts.sum(axis=1), axis=0) * 100

    # Validate results
    pd.testing.assert_frame_equal(round(hotel_cancel_perc, 2), expected_perc)
