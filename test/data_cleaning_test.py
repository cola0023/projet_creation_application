import pytest
import pandas as pd
import pickle
import os
from unittest.mock import patch, MagicMock, mock_open
from script.analysing_data_for_cleaning.data_cleaning import *  # Import all functions


def test_describe_object_columns(capsys) -> None:
    """
    Tests `describe_object_columns` to ensure it displays summary statistics and unique values for object-type columns.

    Parameters:
        capsys: Pytest fixture to capture printed output.

    Returns:
        None
    """
    df: pd.DataFrame = pd.DataFrame({
        'category': ['A', 'B', 'A', 'C'],
        'type': ['X', 'Y', 'X', 'Z'],
        'numeric': [1, 2, 3, 4]  # This column should be ignored
    })

    # Call the function
    describe_object_columns(df)

    # Capture printed output
    captured = capsys.readouterr()
    output: str = captured.out

    # Expected parts of the output
    expected_summary: str = "Summary statistics for categorical columns:\n"
    expected_describe: str = str(df.describe(include='object'))
    expected_unique_category: str = "Column: category\nUnique values: ['A' 'B' 'C']\n--------------------------------------------------"
    expected_unique_type: str = "Column: type\nUnique values: ['X' 'Y' 'Z']\n--------------------------------------------------"

    # Assertions
    assert expected_summary in output, "The summary statistics header is missing."
    assert expected_describe in output, "The summary statistics for object columns are incorrect."
    assert expected_unique_category in output, "The unique values for 'category' are missing or incorrect."
    assert expected_unique_type in output, "The unique values for 'type' are missing or incorrect."


def test_percent_missing_values() -> None:
    """
    Tests `percent_missing_values` to ensure it calculates and returns the percentage of missing values in each column.

    Returns:
        None
    """
    df: pd.DataFrame = pd.DataFrame({
        'A': [1, 2, None, 4],
        'B': [None, None, 3, 4],
        'C': [1, 2, 3, 4],  # No missing values
        'D': [None, None, None, None]  # 100% missing values
    })

    expected_output: pd.Series = pd.Series({
        'D': 100.0,  # 100% missing
        'B': 50.0,   # 50% missing
        'A': 25.0    # 25% missing
    })

    result: pd.Series = percent_missing_values(df)

    # Assert that the result matches the expected output
    pd.testing.assert_series_equal(result, expected_output, check_dtype=False)


def test_handle_missing_values(mocker) -> None:
    """
    Tests `handle_missing_values` to ensure it handles missing values by generating 'used' columns,
    handling specific replacements, and dropping rows with NaNs.

    Parameters:
        mocker: Pytest-mock fixture for mocking dependencies.

    Returns:
        None
    """
    df: pd.DataFrame = pd.DataFrame({
        'A': [1, 2, None, 4],
        'B': [None, None, 3, 4],
        'meal': ['BB', None, 'SC', 'Undefined'],
        'C': [5, 6, 7, 8],  # No missing values
    })

    mock_percent_missing_values = mocker.patch(
        "script.analysing_data_for_cleaning.data_cleaning.percent_missing_values",
        return_value=pd.Series({
            'B': 50.0,
            'A': 25.0,
            'meal': 0.0
        })
    )

    mock_handle_manualy = mocker.patch(
        "script.analysing_data_for_cleaning.data_cleaning.handle_manualy",
        side_effect=lambda df, col, val1, val2: df.assign(meal=df['meal'].replace(val1, val2))
    )

    expected_df: pd.DataFrame = pd.DataFrame({
        'meal': ['BB', 'SC', 'SC'],  # 'Undefined' replaced with 'SC'
        'C': [5, 7, 8],
        'used_B': [0, 1, 1],
        'used_A': [1, 0, 1],
    })

    result_df: pd.DataFrame = handle_missing_values(df)

    pd.testing.assert_frame_equal(result_df.reset_index(drop=True), expected_df.reset_index(drop=True))
    mock_percent_missing_values.assert_called_once_with(df)
    mock_handle_manualy.assert_called_once_with(df, 'meal', 'Undefined', 'SC')


def test_handle_manualy() -> None:
    """
    Tests `handle_manualy` to ensure it replaces a specified value in a column with another value.

    Returns:
        None
    """
    df: pd.DataFrame = pd.DataFrame({
        'meal': ['BB', 'Undefined', 'SC', 'Undefined'],
        'category': ['A', 'B', 'Undefined', 'C']
    })

    expected_df: pd.DataFrame = pd.DataFrame({
        'meal': ['BB', 'SC', 'SC', 'SC'],
        'category': ['A', 'B', 'Undefined', 'C']
    })

    result_df: pd.DataFrame = handle_manualy(df, variable='meal', modif_value='Undefined', replace_value='SC')

    pd.testing.assert_frame_equal(result_df, expected_df)


def test_handle_duplicates(capsys) -> None:
    """
    Tests `handle_duplicates` to ensure it removes duplicate rows and logs the number of duplicates removed.

    Parameters:
        capsys: Pytest fixture to capture printed output.

    Returns:
        None
    """
    df: pd.DataFrame = pd.DataFrame({
        'A': [1, 2, 2, 4],
        'B': ['a', 'b', 'b', 'd'],
        'C': [1.1, 2.2, 2.2, 4.4]
    })

    expected_df: pd.DataFrame = pd.DataFrame({
        'A': [1, 2, 4],
        'B': ['a', 'b', 'd'],
        'C': [1.1, 2.2, 4.4]
    })

    result_df: pd.DataFrame = handle_duplicates(df)

    captured = capsys.readouterr()
    output: str = captured.out

    pd.testing.assert_frame_equal(result_df.reset_index(drop=True), expected_df.reset_index(drop=True))
    assert "Duplicates removed: 1" in output, "The log message for removed duplicates is incorrect."


def test_handle_inconsistent_data() -> None:
    """
    Tests `handle_inconsistent_data` to ensure it removes rows with inconsistent data (i.e., no guests present).

    Returns:
        None
    """
    df: pd.DataFrame = pd.DataFrame({
        'adults': [2, 0, 1, 0],
        'children': [0, 0, 1, 0],
        'babies': [0, 0, 1, 0],
        'room_type': ['Single', 'None', 'Double', 'None']
    })

    expected_df: pd.DataFrame = pd.DataFrame({
        'adults': [2, 1],
        'children': [0, 1],
        'babies': [0, 1],
        'room_type': ['Single', 'Double']
    })

    result_df: pd.DataFrame = handle_inconsistent_data(df)

    pd.testing.assert_frame_equal(result_df.reset_index(drop=True), expected_df.reset_index(drop=True))

def test_plot_boxplots() -> None:
    """
    No Tests on boxplots functions.
    """

