import pytest
import pandas as pd
import pickle
import os
from unittest.mock import patch, MagicMock, mock_open
from script.run_prediction.predict import *  # Import all functions


def test_make_prediction() -> None:
    """
    Tests the `make_prediction` function for predicting a result using a pre-trained model.

    This test creates a mock model and input data to simulate the prediction process.
    It verifies that the function returns the correct probability as a float.

    Returns:
        None
    """
    # Create an example input DataFrame
    test_entry: pd.DataFrame = pd.DataFrame({
        "feature1": [0.5],
        "feature2": [1.2],
        "feature3": [3.8]
    })

    # Simulate the model
    mock_model = MagicMock()
    mock_model.predict_proba.return_value = [[0.3, 0.7]]  # Probability of 0.7 for class 1

    # Mock the loading of the pickle file
    with patch("script.run_prediction.predict.pickle.load", return_value=mock_model):
        with patch("builtins.open", create=True):  # Mock file opening
            # Call the function
            result: float = make_prediction(test_entry)

            # Check the return type
            assert isinstance(result, float), "The result must be a float."

            # Check the return value
            assert result == 0.7, f"The expected result is 0.7, but got {result}."


def test_import_data(mocker) -> None:
    """
    Tests the `import_data` function for importing input data from a CSV file.

    This test mocks file operations and checks if the function correctly returns the
    DataFrame and the current working directory.

    Args:
        mocker: Pytest's mocker fixture for mocking functions.

    Returns:
        None
    """
    # Simulate the content of a CSV file
    mock_csv_data: str = "feature1,feature2,feature3\n0.5,1.2,3.8\n0.7,1.5,4.1"

    # Mock the open method to simulate a CSV file
    mocker.patch("builtins.open", mock_open(read_data=mock_csv_data))

    # Mock os.getcwd to simulate the current working directory
    mocker.patch("os.getcwd", return_value="/fake/directory")

    # Mock pd.read_csv to return a simulated DataFrame
    expected_df: pd.DataFrame = pd.DataFrame({
        "feature1": [0.5, 0.7],
        "feature2": [1.2, 1.5],
        "feature3": [3.8, 4.1]
    })
    mocker.patch("pandas.read_csv", return_value=expected_df)

    # Call the function
    path: str
    df: pd.DataFrame
    path, df = import_data()

    # Check that the returned path is correct
    assert path == "/fake/directory", f"The expected path is '/fake/directory', but got {path}"

    # Check that the returned DataFrame is correct
    pd.testing.assert_frame_equal(df, expected_df)


def test_arrange_df() -> None:
    """
    Tests the `arrange_df` function to ensure it rearranges DataFrame columns
    in the required order and removes any extraneous columns.

    Returns:
        None
    """
    # Define an input DataFrame with unordered and extra columns
    input_df: pd.DataFrame = pd.DataFrame({
        'market_segment': ['Online', 'Offline'],
        'adults': [2, 1],
        'children': [0, 1],
        'lead_time': [50, 20],
        'previous_bookings_not_canceled': [1, 0],
        'adr': [120.0, 90.5],
        'previous_cancellations': [0, 1],
        'booking_changes': [2, 0],
        'customer_type': ['Transient', 'Contract'],
        'deposit_type': ['Non Refund', 'Refundable'],
        'used_company': [0, 1],
        'stays_in_week_nights': [3, 5],
        'total_of_special_requests': [1, 2],
        'reserved_is_assigned': [1, 0],
        # Extra columns not needed
        'unnecessary_column': ['extra1', 'extra2'],
        'another_irrelevant_column': [42, 24]
    })

    # Define the expected DataFrame with columns rearranged and unnecessary ones removed
    expected_df: pd.DataFrame = pd.DataFrame({
        'lead_time': [50, 20],
        'stays_in_week_nights': [3, 5],
        'adults': [2, 1],
        'children': [0, 1],
        'market_segment': ['Online', 'Offline'],
        'previous_cancellations': [0, 1],
        'previous_bookings_not_canceled': [1, 0],
        'booking_changes': [2, 0],
        'deposit_type': ['Non Refund', 'Refundable'],
        'customer_type': ['Transient', 'Contract'],
        'adr': [120.0, 90.5],
        'total_of_special_requests': [1, 2],
        'used_company': [0, 1],
        'reserved_is_assigned': [1, 0]
    })

    # Call the function
    output_df: pd.DataFrame = arrange_df(input_df)

    # Validate that the output matches the expected DataFrame
    pd.testing.assert_frame_equal(output_df, expected_df)


def test_predict_and_save(mocker) -> None:
    """
    Tests the `predict_and_save` function to ensure it makes predictions and saves the result to a file.

    This test mocks the prediction and file operations to verify that the function behaves as expected.

    Args:
        mocker: Pytest's mocker fixture for mocking functions.

    Returns:
        None
    """
    # Define a mock DataFrame for input
    input_df: pd.DataFrame = pd.DataFrame({
        "feature1": [0.5],
        "feature2": [1.2],
        "feature3": [3.8]
    })

    # Mock the `make_prediction` function to return a fixed prediction
    mock_make_prediction = mocker.patch("script.run_prediction.predict.make_prediction", return_value=1)

    # Mock the open function to intercept file writing
    mocked_open = mocker.patch("builtins.open", mock_open())

    # Define a fake path for the working directory
    fake_path: str = "/fake/directory"

    # Normalize the path for compatibility with Windows or other systems
    expected_file_path: str = os.path.normpath("/fake/directory/output.txt")

    # Call the function
    predict_and_save(input_df, fake_path)

    # Check that `make_prediction` was called once with the correct DataFrame
    mock_make_prediction.assert_called_once_with(input_df)

    # Check that the correct file was opened in write mode with the normalized path
    mocked_open.assert_called_once_with(expected_file_path, "w")

    # Check that the correct prediction was written to the file
    mocked_open().write.assert_called_once_with("1")
